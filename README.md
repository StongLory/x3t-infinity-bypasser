# X3T-Infinity Bypasser

This is a very simple HTTP micro-service designed to bypass online checks to X3T-Infinity's server

# Instructions

1. Have Node
2. Run with `node index.js`
3. Locate your hosts file
4. Add the following line `127.0.0.1 x3t-infinity.com`
5. ????
6. PROFIT!!!!